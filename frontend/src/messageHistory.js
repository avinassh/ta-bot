export default [
  { type: 'text', author: `tabot`, data: { text: `Hey there!` } },
  { type: 'text', author: `tabot`, data: { text: `I am an AI powered friendly TA bot for Udacity's Intro to Deep Learning with PyTorch` } },
  { type: 'text', author: `tabot`, data: { text: `Ask me any questions!?` } },
  { type: 'emoji', author: `tabot`, data: { emoji: `😋` } },
]
