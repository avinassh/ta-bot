# Student TA Bot

## How does it work?

It uses Facebook's Infersent model to create sentence embeddings of the existing data. When a new text is queried, it calculates the cosine distance between the the query text and the existing embeddings. The highest value is considered as a match and returned as the answer.

## Requirements

### System Requirements

- Python 3

### Word Vectors

run `setup.sh` to get all the word vectors. This project uses GloVe:
    
    ./setup.sh


### Python Requirements

Check `requirements.txt`:


    pip install -r requirements.txt

### Frontend requirements

check `package.json`:

    npm install

## Deployment


For training, add any new data in `data/` directory. Then run the training to save the embeddings in `embeddings/` dir:

    python train.py


Once that is done, run the backend server:

    python server.py


## Data

Some of the data I have added manually and some from the [FAQ repo of the course](https://github.com/ishgirwan/faqs_pytorch_scholarship).